<?php
    defined('FFMPEG_BINARY') or define('FFMPEG_BINARY', '/usr/bin/ffmpeg');

    final class MetaData {
        private $duration;
        private $title;

        public function __construct($filepath){
            $out = shell_exec(FFMPEG_BINARY." -i \"".$filepath."\" 2>&1");       // var_dump($out);  
            $result = array_slice(explode("\n", $out), 0, -2);     
                      
            do {
               $l =  array_shift($result);
            } while(strpos($l, 'Input') === false);
                                                         
            $meta = array();
            foreach($result as $line){         
                $key = trim(substr($line, 1, strpos($line, ':') - 1));  
                $value = trim(substr($line, strpos($line, ':') + 1, strlen($line)));    
                if (!isset($meta[$key]))
                    $meta[$key] = $value;
            }
            
            $meta['Duration'] = substr($meta['Duration'], 0, strpos($meta['Duration'], '.'));
            
            $t = explode(':', $meta['Duration']);
            $this->duration = $t[0]*3600+$t[1]*60+$t[2];       
            
            if(isset($meta['title'])||isset($meta['artist'])){
                $this->title = ((isset($meta['artist']))?$meta['artist']:"Unknown artist") . " - " .
                  ((isset($meta['title']))?$meta['title']:"Unknown title"); 
            }else{
                $this->title = pathinfo($filepath, PATHINFO_FILENAME);
            }
        }

        public function getDuration(){
            return $this->duration;
        }

        public function getTitle(){
            return $this->title;
        }
}