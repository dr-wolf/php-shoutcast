<?php
	final class Protocol {

		private $metasupport = false;
		private $metaint = 0;

		private $metabytes = '';

		private $nleft = 0;

		public function __construct() {
			$this -> metaint = 8 * 1024;

			set_time_limit(0);

			$this -> metasupport = ($this -> metaint > 0) && (bool)((int)(@$_SERVER['HTTP_ICY_METADATA']));

			header('HTTP/1.0 200 OK');

			header('Content-Type: audio/mpeg', true);
			header('Cache-Control: no-cache', true);
			header('Pragma: no-cache', true);
			header('Expires: Mon, 26 Jul 1997 05:00:00 GMT', true);
			header('Connection: Keep-Alive', true);

			header("ICY-Pub: 1", true);

			if ($this -> metasupport) {
				header("ICY-Metaint: {$this -> metaint}", true);
				$this -> metabytes = "\0";
				$this -> nleft = $this -> metaint;
			}
		}

		public function audioInfo($bitrate, $samplerate, $channels, $genre = null) {
			if ($bitrate) {
				header("ICY-BR: {$bitrate}", true);
				if ($samplerate && $channels) {
					header("ICE-Audio-Info: bitrate={$bitrate};samplerate={$samplerate};channels={$channels}", true);
				}
			}
			if ($genre) {
				header("ICY-Genre: {$genre}", true);
			}
		}

		public function server($serverName) {
			if ($serverName) {
				header("Server: {$serverName}", true);
			}
		}

		public function station($name, $description = null, $url = null) {
			if ($name) {
				header("ICY-Name: {$name}", true);
			}
			if ($description) {
				header("ICY-Description: {$description}", true);
			}
			if ($url) {
				header("ICY-Url: {$url}", true);
			}
		}

		public function metadata($streamTitle) {
			if (!$this -> metasupport) return;

			$streamTitle = trim(str_replace("'", '`', $streamTitle));
			if ($streamTitle != '') {
				$bytes = "StreamTitle='" . $streamTitle . "'";
				$n = strlen($bytes);
				if ($n % 16 != 0) {
					$n = 16 * (int)(($n + 15) / 16);
					$bytes = str_pad($bytes, $n, "\0", STR_PAD_RIGHT);
				}
				$this -> metabytes = chr((int)($n / 16)) . $bytes;
			} else {
				$this -> metabytes = "\0"; // zero-length data
			}
		}

		public function send($buffer) {
			if ($this -> metasupport) {
				while (strlen($buffer) > 0) {
					$chunk = substr($buffer, 0, $this -> nleft);

					$nchunk = strlen($chunk);
					$buffer = substr_replace($buffer, '', 0, $nchunk);

					$this -> nleft -= $nchunk;

					echo $chunk;
					if ($this -> nleft <= 0) {
						$this -> nleft = $this -> metaint;
						echo $this -> metabytes;
					}
					flush();
				}
			} else {
				echo $buffer;
				flush();
			}
		}

	}