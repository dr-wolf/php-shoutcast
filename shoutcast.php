<?php
    require_once('protocol.php');
    require_once('ffmpeg.php');  

    final class Shoutcast {

        private $running = false;

        private $protocol = null;

        private $files = array();

        private $transport = null;

        private $source = null;

        private function init($shift){
            $current = reset($this -> files);
            if ($shift > 0)
                do {
                    if ($shift < $current['duration']) break;
                    $shift -= $current['duration'];
                } while($current = next($this -> files));
            $this -> source = new FFMpeg($current['path'], $shift);
            $this -> protocol -> metadata($current['title']);
        }

        private function next() {
            if (($current = next($this -> files)) === false) {
                $current = reset($this -> files);
            }
            $this -> source = new FFMpeg($current['path']);
            $this -> protocol -> metadata($current['title']);
        }

        public function __construct($files = null) {
            if($files){
                $this -> files = $files;
                reset($this -> files);
            }
        }

        protected function skip($kbytes) {
            $this -> source -> data($kbytes * 1024);
        }       

        protected function data() {
            if (!$this -> source) return false;
            return $this -> source -> data(8 * 1024);
        }

        public function run($shift = 0) {
            if ($this -> running) return;
            $this -> running = true;
            $this -> protocol = new Protocol();
            if (count($this -> files) > 0) {
                $this -> init($shift);
                while(!connection_aborted()) {
                    if (gc_enabled()) gc_collect_cycles();
                    while (($chunk = $this -> data()) !== false) {
                        $this -> protocol -> send($chunk);
                        if (connection_aborted()) break;
                        sleep(0);
                    }
                    $this -> next();     
                }
                $this -> source = null;
                $this -> files = array();
            }
            $this -> protocol = null;
            $this -> running = false;
        }

}