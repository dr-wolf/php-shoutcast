<?php
    defined('FFMPEG_BINARY') or define('FFMPEG_BINARY', '/usr/bin/ffmpeg');

    final class FFMpeg {

        private $process = null;
        private $pipe = null;

        static protected $options = array(
            'bitrate' => '96k',
            'samplerate' => '44100',
            'channels' => 2,
        );        

        static public function configure($options) {
            if (is_array($options)) {
                foreach (self::$options as $key => $value) {
                    if (array_key_exists($key, $options)) {
                        self::$options[$key] = $options[$key];
                    }
                }
            }
            return self::$options;
        }

        public function __construct($inputFile, $shift = 0) {
            $mp3_options = self::$options;

            $args = array();

            $args[] = '-loglevel quiet';
            $args[] = '-loglevel info';
            //$args[] = '-re';
            $args[] = '-i "' . $inputFile . '"';
            $args[] = '-map a';
            
            if(!empty($shift)) $args[] = '-ss ' . gmdate("H:i:s", $shift);

            if ($mp3_options['bitrate'])    $args[] = '-ab ' . $mp3_options['bitrate'];
            if ($mp3_options['samplerate']) $args[] = '-ar ' . $mp3_options['samplerate'];
            if ($mp3_options['channels'])   $args[] = '-ac ' . $mp3_options['channels'];

            $args[] = '-f mp3';
            $args[] = '-map_metadata -1';
            $args[] = '-y -';

            $cmd = FFMPEG_BINARY . ' ' . implode(' ', $args);        //var_dump($cmd); die();
        
            $descriptorspec = array(
                0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
                1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
                2 => array("pipe", "w"),  // stderr is a pipe to write to
            );
            $options = array(
                'bypass_shell' => true,
            );

            $this -> process = proc_open($cmd, $descriptorspec, $pipes, null, null, $options);
            if (is_resource($this -> process)) {
                fclose($pipes[0]);
                fclose($pipes[2]);
                $this -> pipe = $pipes[1];
                stream_set_blocking($pipes[1], 0);
            } else {
                $this -> process = null;
            }
        }

        public function __destruct() {
            if ($this -> pipe) {
                fclose($this -> pipe);
                $this -> pipe = null;
            }
            if ($this -> process) {
                proc_terminate($this -> process);
                proc_close($this -> process);
                $this -> process = null;
            }
        }

        public function data($limit = -1) {
            if ($this -> pipe) {
                $status = proc_get_status($this -> process);
                if ($status['running']) {
                    if ($limit < 0) {
                        $result = stream_get_contents($this -> pipe);
                        // We received all the data
                        fclose($this -> pipe); $this -> pipe = null;
                        proc_close($this -> process); $this -> process = null;

                        return $result;
                    } else {
                        return fread($this -> pipe, $limit);
                    }
                } else {
                    fclose($this -> pipe); $this -> pipe = null;
                    proc_close($this -> process); $this -> process = null;
                }
            }
            return false;
        }

        public function valid() {
            return is_resource($this -> process);
        }

}